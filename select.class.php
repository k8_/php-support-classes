<?php

class Select
{
  public $select='';
  public $id;
  public $options;
  public $selected_option;
  public $empty_line;
  public $attributes;

  public function __construct($id,$options,$selected_option=false,$empty_line=false,$attributes=Array())
  {
    $this->id = $id;
    $this->options = $options;
    $this->selected_option = $selected_option;
    $this->empty_line = $empty_line;
    $this->attributes = $attributes;
  }

  public function getSelect()
  {
    $key_comp = 0;

    $this->_setupSelect();

    // set options and mark the correct one as selected
    foreach ($this->options as $key=>$option) {
      // this sets the text and value for each option depending on
      // whether the option is an array of additional values for other purposes,
      // and then whether the $key is a number or an actual value.
      $text = (is_array($option)) ? $key : $option;
      $value = ($key === $key_comp) ? $option : $key;
      $value = htmlspecialchars($value,ENT_QUOTES,false);

      $selected = ($value == $this->selected_option || (!$this->empty_line && $key_comp==0)) ? ' selected="selected"' : '';

      $this->select.= '<option value="'.$value.'"'.$selected.'>'.$text.'</option>';

      $key_comp++;
    }

    $this->select.= '</select>';

    return $this->select;
  }

  public function getRadio()
  {
    $key_comp = 0;

    foreach ($this->options as $key=>$option) {
      $text = (is_array($option)) ? $key : $option;
      $value = ($key === $key_comp) ? $option : $key;
      $value = htmlspecialchars($value,ENT_QUOTES,false);

      $selected = ($value == $this->selected_option) ? ' checked' : '';

      $this->select.='<input type="radio" name="'.$this->id.'" value="'.$value.'"'.$selected.' />'.$text."<br/>\n";

      $key_comp++;
    }

    return $this->select;
  }

  public function getCheckboxes()
  {
    $key_comp = 0;
    $selected = '';

    foreach ($this->options as $key=>$option) {
      $text = (is_array($option)) ? $key : $option;
      $value = ($key === $key_comp) ? $option : $key;
      $value = htmlspecialchars($value,ENT_QUOTES,false);

      $selected = ($value == $this->selected_option) ? ' checked' : '';

      $this->select.='<input type="checkbox" name="'.$this->id.'" value="'.$value.'"'.$selected.' />'.$text."<br/>\n";

      $key_comp++;
    }

    return $this->select;
  }

  /***
    * To create a time select, set $id to your chosen id and $options to the time interval
    * you want to set: 15 for every 15 minutes, 60 for every hour, etc. All optional
    * arguments should work for this.
    *
    * Takes a start and end time as arguments. These should be in military: 15:00, 07:00
    *
    * Returns a select with military times at the selected intervals.
    ***/
  public function createTimeSelect($start,$end)
  {
    $this->_setupSelect();
    $time = new DateTime($start);
    $end_time = new DateTime($end);
    do {
      $selected = ($time == $this->selected_option) ? ' selected="selected"' : '';

      $this->select.= '<option value="'.$time->format('H:i').':00"'.$selected.'>'.$time->format('Hi').'</option>';
      $time->add(new DateInterval('P0DT'.$this->options.'M'));
    } while ($time->format('U')<=$end_time->format('U'));

    return $this->select;
  }

  protected function _setupSelect()
  {
    // make select opening statement
    $this->select = '<select id="'.$this->id.'" name="'.$this->id.'"';
    if (is_array($this->attributes)) {
      foreach ($this->attributes as $name=>$value) {
        $this->select.= ' '.$name.'="'.htmlspecialchars($value,ENT_QUOTES,false).'"';
      }
    }

    $this->select.=  empty($this->selected_option) ? '' : ' existing="'.$this->selected_option.'"';

    $this->select.= ">\n";

    if ($this->empty_line == true) {
      if (empty($this->selected_option)) {
        $this->select.='<option value="" selected=""></option>';
      } else {
        $this->select.= '<option value=""></option>';
      }
    }
  }
}
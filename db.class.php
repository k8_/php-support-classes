<?php

use PDO;

/***
 * Basic class to create and a db instance.
 * For now all data is stored in the class, but this can be altered as needed.
 */
class DBConnect {
	protected $_db_host;
	protected $_db_name;
	protected $_db_user;
	protected $_db_pwd;

	public function __construct()
	{
		$this->_db_host = 'localhost'; // change for live server
		$this->_db_name = 'orwhabstracts';
		$this->_db_user = 'orwhabstracts'; // change for live server
		$this->_db_pwd = 'June15th';

    return $this->getConnection();
	}

	public function getConnection()
	{
	  try {
      $conn = new PDO('mysql:host='.$this->_db_host.';dbname='.$this->_db_name,$this->_db_user,$this->_db_pwd);

      return $conn;
    }
    catch (PDOException $e) {
      echo 'Cannot connect to database';
      exit;
    }
  }
}
?>
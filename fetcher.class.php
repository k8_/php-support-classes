<?php
include_once('db.class.php');

/***
 * based on the Data Mapper pattern from Java
 *
 * This class serves as a basis for all of the database interactions for audservices.
 *
 * Setup:
 * 1. set $_baseSql, $class_name, and $class_file
 * 2. set up _setItem: create sql, prepare the statement and bind parameters
 * 3. set up public get queries that will reply on _getItem.
 */
abstract class Fetcher
{
  protected $conn;
  protected $_baseSql; // set in each class, up to WHERE _with_ extra space
  public $class_name;
  public $class_file;

  public function __construct()
  {
    $db = new NRL_DBConnect();
    $this->conn = $db->getConnection();
  }

  protected function _getItem($sql)
  {
    try {
      $stmt = $this->conn->prepare($sql);
      $stmt->execute();

      $error=$stmt->errorInfo();
      if ($error[0] == 0) {
        require_once($this->class_file);
        $results = $stmt->fetchAll(PDO::FETCH_CLASS,$this->class_name);

      } else {
        $error_message = $error[2];
      }
    } catch(Exception $e) {
      $error_message = $e;
    }

    if (isset($error_message)) {
      return $error_message;
    } else {
      return $results;
    }
  }

  protected function _setItem($stmt)
  {
  	// set up sql
  	// prepare statement
  	// bind parameters

    $stmt->execute();
    $error = $stmt->errorInfo();

    if ($error[0] == 0) {
      $last_id = $this->conn->lastInsertId();

      // add or edit pictures here
      return 1;
    } else {
      return $error[2];
    }
  }
}
<?php
class NRL_Site
{
  protected $_db_host;
  protected $_db_name;
  protected $_db_user;
  protected $_db_pwd;
  public $base_path;
  public $t_path;
  public $c_path;
  public $conn;

  public function __construct()
  {
    $this->_db_host = 'mysql-priv.ccs.nrl.navy.mil';
    $this->_db_name = 'audservices';
    $this->_db_user = 'audservices';
    $this->_db_pwd = 'Q5pA8xav';

    $this->base_path = '/afs/ccs/web/audservices/pipeline/htdocs/';
    $this->t_path = $this->base_path.'pages/';
    $this->c_path = $this->base_path.'controllers/';

    $this->conn = $this->_getConnection();
  }

  protected function _getConnection()
  {
    try {
      $conn = new PDO('mysql:host='.$this->_db_host.';dbname='.$this->_db_name,$this->_db_user,$this->_db_pwd);

      return $conn;
    }
    catch (PDOException $e) {
      echo 'Cannot connect to database';
      exit;
    }
  }

 /**
  * Initializes LDAP connection through configuration options
  *
  * @param object config variables
  *
  *  Assumed:
  *   $cfg->ldap_host="ldaps://ldap.nrl.navy.mil";
  * $cfg->ldap_port=636;
  * $cfg->ldap_dn="uid=ccsldap,o=specialaccounts";
  * $cfg->ldap_pass="CL52Rdah";
  *
  * @returns object resource identifier
  **/
  protected function _ldapinit($cfg){
    if(isset($cfg->ldap_host) && isset($cfg->ldap_port)){
      ldap_set_option(null,LDAP_OPT_TIMELIMIT,10);
      $ldc = @ldap_connect($cfg->ldap_host, $cfg->ldap_port);
      ldap_set_option($ldc, LDAP_OPT_PROTOCOL_VERSION, 3);

      if(!$ldc) {
        $this->errors="Error: LDAP ".ldap_errno($ldc)."\nDescription: ".ldap_error($ldc);
        return false;
      }else{
        $ldb = @ldap_bind($ldc, $cfg->ldap_dn, $cfg->ldap_pass);
        if(!$ldb){
          $this->errors="Error: LDAP ".ldap_errno($ldc)."\nDescription: ".ldap_error($ldc);
          return false;
        }else{
          return $ldc;
        }
      }
    }
  }

  /**
  * Retrieves the EDIPI from the SERVER variable
  *
  * @params none
  * @returns string EDPI code
  **/
  protected function _get_edipi(){
    if(isset($_SERVER['SSL_CLIENT_S_DN_CN'])){
      $cac_array = explode('.',$_SERVER['SSL_CLIENT_S_DN_CN']);
      $edipi=array_pop($cac_array);
      if($edipi<=0){
        return null;
      }else{
        return $edipi;
      }
    }
  }


  /**
  * Opens template file and parses for tags {tag}
  *
  * @param string file path of template file
  * @param array key value pairs of tags to find and replace
  *
  * @returns nothing
  **/
  public function parse_template($tmpl,$vars,&$result=""){
    $eval = false;
    $no_echo = true;
    if(file_exists($tmpl)){
      $this->_rootref = array();

      foreach ($vars as $key => $val)
      {
        $this->_rootref[$key] = $val;
      }

      $contents=file_get_contents($tmpl);

      preg_match_all("/\{(.[^\}|^\s]*)\}/siU", $contents, $tags, PREG_PATTERN_ORDER);

      if(isset($tags[1])){
        foreach($tags[1] as $tag){
          $lower_tag = strtolower($tag);
          $replace=isset($vars->$tag) ? $vars->$tag : "";
          if(!is_bool($replace) && preg_match('/(\<|\>|\[|\]|\{|\})/',$replace)==0){
            $replace = htmlentities($replace,ENT_COMPAT,'UTF-8',false);
          }
          $contents=str_replace("{".$tag."}",$replace,$contents);
        }
      }

      preg_match_all('#<!-- (if|else|elseif|endif) (.*?)? ?-->#', $contents, $blocks, PREG_SET_ORDER);
      $text_blocks = preg_split('#<!-- [^<].*? (?:.*?)? ?-->#', $contents);

      for ($curr_tb = 0, $tb_size = sizeof($blocks); $curr_tb < $tb_size; $curr_tb++)
      {
        $block_val = &$blocks[$curr_tb];

        switch ($block_val[1])
        {
          case 'if':
            $eval = true;
            $compile_blocks[] = '<?php '.$this->compile_tag_if($block_val[2],false).' ?>';
            break;
          case 'else':
            $eval = true;
            $compile_blocks[] = '<?php } else { ?>';
            break;
          case 'elseif':
            $eval = true;
            $compile_blocks[] = '<?php '.$this->compile_tag_if($block_val[2],true).' ?>';
            break;
          case 'endif':
            $eval = true;
            $compile_blocks[] = '<?php } ?>';
            break;
          default:
            $trim_check = trim($block_val[0]);
            $compile_blocks[] =
              (!$no_echo) ? (
                (!empty($trim_check)) ? $block_val[0] : '') : (
                (!empty($trim_check)) ? $block_val[0] : '');
            break;
        }
      }

      $template_php = '';
      for ($i = 0, $size = sizeof($text_blocks); $i<$size; $i++)
      {
        $trim_check_text = trim($text_blocks[$i]);
        $template_php.=
          (!$no_echo) ? (
            ($trim_check_text != '') ? $text_blocks[$i] : ''
          ).(
            (isset($compile_blocks[$i])) ? $compile_blocks[$i] : ''
          ) : (
            ($trim_check_text != '') ? $text_blocks[$i] : ''
          ).(
            (isset($compile_blocks[$i])) ? $compile_blocks[$i] : ''
          );
      }

      if($eval){
        ob_start();
        eval(' ?>' . str_replace(' ?><?php ', ' ', $template_php) . '<?php ;');
        $contents = ob_get_clean();
      } else {
        $contents = $template_php;
      }

      $result=$contents;
      $this->_content=$contents;

    }else{
      $this->errors="Error: 0\nDescription: Template file ($tmpl) does not exist\nFile:".__FILE__."\nLine:".__LINE__;

      die($this->errors);
    }
  }

  /**
  * Outputs contents of content buffer and then exits
  *
  * @param none
  * @returns nothing
  **/
  public function output(){
    ob_end_clean();
    echo $this->_content;
    ob_end_flush();
    $this->_content="";
    $this->_cache=Array();
    exit;
  }

  public function compile_tag_if($tag_args, $elseif)
  {
    // Tokenize args for 'if' tag.
    preg_match_all('/(?:
      "[^"\\\\]*(?:\\\\.[^"\\\\]*)*"         |
      \'[^\'\\\\]*(?:\\\\.[^\'\\\\]*)*\'     |
      [(),]                                  |
      [^\s(),]+)/x', $tag_args, $match);

    $tokens = $match[0];
    $is_arg_stack = array();

    // go through each part of the statement.
    for ($i = 0, $size = sizeof($tokens); $i < $size; $i++)
    {
      $token = &$tokens[$i];

      switch ($token)
      {
        case '!==':
        case '===':
        case '<<':
        case '>>':
        case '|':
        case '^':
        case '&':
        case '~':
        case ')':
        case ',':
        case '+':
        case '-':
        case '*':
        case '/':
        case '@':
        // don't do anything
        break;

        case '==':
        case 'eq':
        case 'is':
          $token = '==';
        break;

        case '!=':
        case '<>':
        case 'ne':
        case 'neq':
          $token = '!=';
        break;

        case '<':
        case 'lt':
          $token = '<';
        break;

        case '<=':
        case 'le':
        case 'lte':
          $token = '<=';
        break;

        case '>':
        case 'gt':
          $token = '>';
        break;

        case '>=':
        case 'ge':
        case 'gte':
          $token = '>=';
        break;

        case '&&':
        case 'and':
          $token = '&&';
        break;

        case '||':
        case 'or':
          $token = '||';
        break;

        case '!':
        case 'not':
          $token = '!';
        break;

        case '%':
        case 'mod':
          $token = '%';
        break;

        case '(':
          array_push($is_arg_stack, $i);
        break;

        // any argument that's not listed above is changed to lowercase
        default:
          if (preg_match('#^((?:[A-Z0-9\-_]+\.)+)?(\$)?(?=[a-z])([a-z0-9\-_]+)#s', $token, $varrefs))
          {
            // set any referenced variables used in conditionals to null if they're not set
            if(!isset($this->_rootref[$varrefs[3]])) {
              $this->_rootref[$varrefs[3]] = null;
            }
            $token = '$this->_rootref[\'' . $varrefs[3] . '\']';

          }
          else if (preg_match('#^\.((?:[a-z0-9\-_]+\.?)+)$#s', $token, $varrefs))
          {
            // Allow checking if loops are set with .loopname
            // It is also possible to check the loop count by doing <!-- IF .loopname > 1 --> for example
            $blocks = explode('.', $varrefs[1]);

            // If the block is nested, we have a reference that we can grab.
            // If the block is not nested, we just go and grab the block from _tpldata
            if (sizeof($blocks) > 1)
            {
              $block = array_pop($blocks);
              $namespace = implode('.', $blocks);
              $varref = $this->generate_block_data_ref($namespace, true);

              // Add the block reference for the last child.
              $varref .= "['" . $block . "']";
            }
            else
            {
              $varref = '$this->_tpldata';

              // Add the block reference for the last child.
              $varref .= "['" . $blocks[0] . "']";
            }
            $token = "sizeof($varref)";
          }
        break;
      }
    }
    $return = (($elseif) ? '} else if (' : 'if (') . (implode(' ', $tokens) . ') { ');
    return $return;
  }

}